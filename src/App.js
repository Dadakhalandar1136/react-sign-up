import React from 'react';
import './App.css';
import SignUp from './component/SignUp';

class App extends React.Component {

  render() {
    return (
      <div className='App'>
        <SignUp />
      </div>
    )
  }
}

export default App;
