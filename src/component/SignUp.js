import React from 'react'
import validator from 'validator';
import './SignUp.css';

class SignUp extends React.Component {
    constructor() {
        super();

        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            gender: "",
            eMail: "",
            password: "",
            repeatPassword: "",
            role: "",
            termsConditions: false,
            success: "",
            error: {
                errorFirstName: "",
                errorLastName: "",
                errorAge: "",
                errorGender: "",
                errorMail: "",
                errorPassword: "",
                errorRepeatPassword: "",
                errorRole: "",
                errorTermsConditions: "",
                errorSuccess: "",
            }
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let errors = {};
        let count = 0;


        if (this.state.firstName.length === 0) {
            errors.errorFirstName = "Please enter your first name!";
        } else if (!validator.isAlpha((this.state.firstName).trim())) {
            errors.errorFirstName = "This field only contain Alphabets";
        } else {
            count++;
        }

        if (this.state.lastName.length === 0) {
            errors.errorLastName = "Please enter your last name!"
        } else if (!validator.isAlpha((this.state.lastName).trim())) {
            errors.errorLastName = "This field only contain Alphabets";
        } else {
            count++;
        }

        if (this.state.age.length === 0) {
            errors.errorAge = "Please enter your age";
        } else if (!validator.isInt(this.state.age)) {
            errors.errorAge = "This field contain only numbers";
        } else if (this.state.age < 18 || this.state.age > 65) {
            errors.errorAge = "Age must be 18 to 65";
        } else {
            count++;
        }

        if (this.state.gender === "") {
            errors.errorGender = "Please select Gender";
        } else {
            count++;
        }

        if (this.state.eMail.length === 0) {
            errors.errorMail = "Please enter your valid Email";
        } else if (!validator.isEmail(this.state.eMail)) {
            errors.errorMail = "Example: exmaple@gmail.com";
        } else {
            count++;
        }

        if (this.state.role === "1" || this.state.role === "") {
            errors.errorRole = "Please select Role";
        } else {
            count++;
        }

        if (!validator.isStrongPassword(this.state.password)) {
            let message = ["Must contains 1 upper case", "Must contains 1 lower case", "Must contains 1 number", "Must contains 1 symbol"];
            let messageValues = (this.state.password).split("").map((char) => {
                if (isNaN(char)) {
                    if (!validator.isAlphanumeric(char)) {
                        message.splice(3, 1, "")
                        return 5;
                    } else if (validator.isLowercase(char)) {
                        message.splice(1, 1, "")
                        return 3;
                    } else if (validator.isUppercase(char)) {
                        message.splice(0, 1, "");
                        return 2;
                    } else {
                        return 6;
                    }
                } else {
                    message.splice(2, 1, "");
                    return -1;
                }
            });
            if (messageValues.length < 8) {
                message.push("Length must be greater than 7")
            }
            errors.errorPassword = message.join(" ");

        } else {
            count++;
        }

        if (this.state.password !== this.state.repeatPassword) {
            errors.errorRepeatPassword = "Password Mismatched";
        } else {
            count++;
        }

        if (this.state.termsConditions === false) {
            errors.errorTermsConditions = "Please click on the to agree our terms and conditions";
        } else {
            count++;
        }
        
        if (count === 9) {
            errors.errorSuccess = "Success";
        }

        this.setState({ error: errors });
    }

    render() {
        return (
            <section className="min-vh-100 gradient-custom">
                <div className="container py-5">
                    <div className="row justify-content-center align-items-center h-100">
                        <div className="col-12 col-lg-10 col-xl-7 bg-light">
                            <div className="shadow-lg p-3 mb-5 bg-body-tertiary rounded">
                                <div className="card-body p-4 p-md-3">
                                    <h3 className="h3 mb-3 pb-2 pb-md-0 mb-md-5">REGISTRATION FORM</h3>
                                    <form onSubmit={this.handleSubmit}>

                                        <div className="row">
                                            <div className="col-md-6 mb-4">

                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="firstName"> <i className="fa-solid fa-user"></i> First Name </label>
                                                    <input type="text" id="firstName" className="form-control form-control-lg" placeholder="First Name" onChange={(event) => {
                                                        return this.setState({ firstName: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.errorFirstName}</small>
                                                </div>

                                            </div>

                                            <div className="col-md-6 mb-4">

                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="lastName"> <i className="fa-solid fa-user"></i> Last Name</label>
                                                    <input type="text" id="lastName" className="form-control form-control-lg" placeholder="Last Name" onChange={(event) => {
                                                        return this.setState({ lastName: event.target.value })
                                                    }} />
                                                    <small>{this.state.error.errorLastName}</small>
                                                </div>

                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-6 mb-4 d-flex align-items-center">

                                                <div className="form-outline datepicker w-100">
                                                    <label htmlFor="age" className="h5 form-label"> <i className="fa-solid fa-person-cane"></i> Age</label>
                                                    <input type="text" className="form-control form-control-lg" id="age" placeholder="Age" onChange={(event) => {
                                                        return this.setState({ age: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.errorAge}</small>
                                                </div>

                                            </div>

                                            <div className="col-md-6 mb-4">

                                                <h5 className="mb-2 pb-1 h5"><i className="fa-solid fa-transgender"></i> Gender: </h5>

                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="femaleGender"
                                                        value="option1" checked={this.state.gender === "option1"} onChange={(event) => {
                                                            return this.setState({ gender: event.target.value });
                                                        }} />
                                                    <label className="form-check-label" htmlFor="femaleGender">Female</label>
                                                </div>

                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="maleGender"
                                                        value="option2" checked={this.state.gender === "option2"} onChange={(event) => {
                                                            return this.setState({ gender: event.target.value });
                                                        }} />
                                                    <label className="form-check-label" htmlFor="maleGender">Male</label>
                                                </div>

                                                <div className="form-check form-check-inline">
                                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="otherGender"
                                                        value="option3" checked={this.state.gender === "option3"} onChange={(event) => {
                                                            return this.setState({ gender: event.target.value });
                                                        }} />
                                                    <label className="form-check-label" htmlFor="otherGender">Other</label>
                                                </div>

                                                <div>
                                                    <small>{this.state.error.errorGender}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-6 mb-4 pb-2">

                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="emailAddress"> <i className="fa-solid fa-envelope"></i> Email</label>
                                                    <input type="email" id="emailAddress" className="form-control form-control-lg" placeholder="Email" onChange={(event) => {
                                                        return this.setState({ eMail: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.errorMail}</small>
                                                </div>

                                            </div>

                                            <div className="col-md-6 mb-4 pb-2">

                                                <div className="d-flex flex-column">
                                                    <label className="h5 form-label select-label">Please Select Role</label>
                                                    <select className="select form-control-lg drop-down" onChange={(event) => {
                                                        return this.setState({ role: event.target.value })
                                                    }}>
                                                        <option value="1" >Select Role</option>
                                                        <option value="2" >Developer</option>
                                                        <option value="3" >Senior Developer</option>
                                                        <option value="4" >Lead Engineer</option>
                                                        <option value="5" >CTO</option>
                                                    </select>
                                                    <small>{this.state.error.errorRole}</small>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">

                                            <div className="col-md-6 mb-4 pb-2">
                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="password"><i className="fa-solid fa-lock"></i> Password</label>
                                                    <input type="password" id="password" autoComplete="on" className="form-control form-control-lg" placeholder="New Password" onChange={(event) => {
                                                        return this.setState({ password: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.errorPassword}</small>
                                                </div>

                                            </div>

                                            <div className="col-md-6 mb-4 pb-2">
                                                <div className="form-outline">
                                                    <label className="h5 form-label" htmlFor="repeat-password"><i className="fa-solid fa-lock"></i> Repeat Password</label>
                                                    <input type="password" id="repeat-password" autoComplete="on" className="form-control form-control-lg" placeholder="Re-enter Password" onChange={(event) => {
                                                        return this.setState({ repeatPassword: event.target.value });
                                                    }} />
                                                    <small>{this.state.error.errorRepeatPassword}</small>
                                                </div>

                                            </div>
                                        </div>

                                        <div className="row bottom-container">
                                            <div className="col-6">
                                                <div className="form-check d-flex justify-content-center mt-3">
                                                    <div className="d-flex justify-content-end">
                                                        <input className="form-check-input me-2" type="checkbox" id="check-box" onChange={(event) => {
                                                            return this.setState({ termsConditions: event.target.checked });
                                                        }} />
                                                        <label className="form-check-label" htmlFor="check-box">
                                                            Agree to <a href="#!" className="text-body"><u>terms and conditions</u></a>
                                                        </label>
                                                    </div>
                                                </div>
                                                <small>{this.state.error.errorTermsConditions}</small>
                                            </div>
                                            <div className="col-6">
                                                <div className="pt-2">
                                                    <input className="btn btn-primary btn-md px-5 fs-5 fw-bold" type="submit" value="Submit" />
                                                </div>
                                            </div>
                                        </div>
                                        <h1>{this.state.error.errorSuccess}</h1>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default SignUp